<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Test extends CI_Controller
{
	
	public function oss()
	{
		echo 'oss';
		$this->output->set_output('aaa');
	}

	public function voices()
	{
		sleep(1);
		$arr = array(
			array(
				'id' => 8,
				'name' => 'voice_8_name',
				'author' => 'my',
				'link' => '/listen/8'
			),
			array(
				'id' => 9,
				'name' => 'voice_9_name',
				'author' => 'my',
				'link' => '/listen/9'
			),
			array(
				'id' => 10,
				'name' => 'voice_9_name',
				'author' => 'my',
				'link' => '/listen/10'
			),
			array(
				'id' => 11,
				'name' => 'voice_9_name',
				'author' => 'my',
				'link' => '/listen/11'
			),
			array(
				'id' => 12,
				'name' => 'voice_9_name',
				'author' => 'my',
				'link' => '/listen/12'
			),
			array(
				'id' => 13,
				'name' => 'voice_9_name',
				'author' => 'my',
				'link' => '/listen/13'
			),
			array(
				'id' => 14,
				'name' => 'voice_9_name',
				'author' => 'my',
				'link' => '/listen/14'
			),
			array(
				'id' => 15,
				'name' => 'voice_9_name',
				'author' => 'my',
				'link' => '/listen/15'
			)
		);
		$arr2 = array(
			8 => 'https://ui-7.cn/voice/qingshixiang.mp3',
			9 => 'https://ui-7.cn/voice/201711221837466169.mp3',
			10 => 'http://127.0.0.101/video/qingshixiang.mp3',
			11 => 'http://127.0.0.101/video/aaa.mp3',
			12 => 'http://127.0.0.101/video/qingshixiang.mp3',
			13 => 'http://127.0.0.101/video/qingshixiang.mp3',
			14 => 'http://127.0.0.101/video/qingshixiang.mp3',
			15 => 'http://127.0.0.101/video/qingshixiang.mp3'
		);
		header("Access-Control-Allow-Origin: * ");
		$this->output->set_output(json_encode(array('list'=>$arr, 'voice'=>$arr2)));
	}

	public function voicesCount()
	{
		header("Access-Control-Allow-Origin: * ");
		$this->output->set_output(json_encode(array('status'=>200, 'info'=>'yes', 'posts'=>100)));
	}
}